#!/usr/bin/env python

import socket
import threading
import sys
import time
import re
import json
import requests
import auth
import io

server = "irc.twitch.tv"       #settings
channel = "#gilae"
botnick = "oswaldbot"
with io.open('oauth.txt','r') as authfile:
	pword = authfile.read().replace('\n','')

irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #defines the socket
print "connecting to:"+server
irc.connect((server, 6667))                                                         #connects to the server
irc.send("PASS "+ pword +"\n")
irc.send("NICK "+ botnick +"\n")                            #sets nick
irc.send("PRIVMSG nickserv :iNOOPE\r\n")    #auth
irc.send("CAP REQ :twitch.tv/membership\r\n")
irc.send("CAP REQ :twitch.tv/tags\r\n")
irc.send("JOIN "+ channel +"\n")        #join the chan
tstflood = 0
linkspermitted = 0
rankflood = 0
runeflood = 0
cprefix = '#gilae :'
with io.open('token.txt','r') as tokenfile:
	tok=tokenfile.read().replace('\n','')
aheaders = {'Authorization':'OAuth ' +tok,'Accept':'application/vnd.twitchtv.v3+json'}
bheaders = {'Accept':'application/vnd.twitchtv.v3+json'}
url = 'https://api.twitch.tv/kraken/channels/gilae'
baseriot = 'https://na.api.pvp.net/api/lol/'
with io.open('apikey.txt','r') as keyfile:
	apikey=keyfile.read().replace('\n','')
mysummname = 'Unicorn Giggles1'
summnametag = 'unicorngiggles1'
mysummid = '21447001'
while 1:   
   sender = ''
   los = ''
   text=irc.recv(2040) 
   print text   #print text to console
   if text.find('PING') != -1:                        
      irc.send('PONG ' + text.split() [1] + '\r\n')
   elif text.find(cprefix) != -1:
      message = text.split(cprefix)
      for i in range(1,len(message)):
	 los = los + message[i]
      usersplit = text.split(' :')
      userstate = usersplit[0]
      userlong = usersplit[1]
      usershortened = userlong.split('!')
      sender = usershortened[0]
      #print(los+'-'+sender)
      ctime = time.time()
      if re.search('^!spurge(\r)*(\n)$', los) != None and tstflood < ctime:
         tstflood = ctime + 30
         #t = text.split(':!test')
         #sender = t[1].strip()
         irc.send('PRIVMSG ' +channel+' :/timeout '+str(sender)+' 1 \r\n')
         irc.send('PRIVMSG ' +channel+' :/me Well, you asked for it '+str(sender)+'. \r\n')
      if re.search('^!permit(\r)*(\n)$', los) != None and re.search('mod=1',userstate) != None:
         linkspermitted = ctime + 30 
         irc.send('PRIVMSG ' +channel+' :/me You may post your sinful links for 30 seconds \r\n')
      if re.search('.+\.([Cc][Oo][Mm]|[Cc][Aa]|[Cc][Oo]|[Uu][Kk]|[Gg][Oo][Vv]|[Tt][Vv]|[Oo][Rr][Gg]|[Ll][Yy]|[Ss][Hh]).*', los) != None and linkspermitted < ctime and re.search('mod=1',userstate) == None and re.search('tmi.twitch.tv', los) == None:
         irc.send('PRIVMSG ' +channel+' :/me Request permission to post links, '+str(sender)+'! \r\n')
         irc.send('PRIVMSG ' +channel+' :/timeout '+str(sender)+ ' 1 \r\n')
      if re.search('^[^0-9a-zA-Z!@#$%\^/\[\]\*\\\(\)\-\+\=\~\`]ACTION ',los) != None and re.search('mod=1',userstate) == None:
         irc.send('PRIVMSG '+channel+' :/me Only mods may use the \'/me\' twitch command \r\n')
         irc.send('PRIVMSG '+channel+' :/timeout '+str(sender)+ ' 1 \r\n')
      if re.search('.*[Nn][Ii][Gg][Gg][EeAa][Rr]*.*', los) != None:
         irc.send('PRIVMSG ' +channel+' :/timeout '+str(sender)+' 90 \r\n')
         #irc.send('PRIVMSG ' +channel+' :/me Thou hast made thyself clear, '+str(sender)+ ', and thou leaveth me no choice, repent for your sins!')
         irc.SEND('PRIVMSG ' +channel+' :/me User '+str(sender)+ ' has been timed out for 90 seconds.')
      if re.search('^!playing(\r)*(\n)$', los) != None:
         dict1 = requests.get(url, headers = aheaders)
         res = dict1.text
         #dict2 = requests.get('https://api.twitch.tv/kraken', headers = aheaders)
         #dict2.raise_for_status
         #print(json.loads(dict2.text)['token'] ['authorization'])
         #print(json.loads(res)['game'])
         irc.send('PRIVMSG ' +channel+' :/me Gilae is playing '+json.loads(res)['game']+'\r\n')
         #parsed_info = json.loads(cinfo)
         #irc.send('PRIVMS ' +channel+' :/me ' +parsed_info['status']+' \r\n')
      if re.search('^!setgame .*',los) and re.search('mod=1',userstate) != None:
         game = los.strip('!setgame ')
         game = game.strip('\r\n')
         params = 'channel[game]='+game
         #print(params)
         requests.put(url, headers = aheaders, params = params).raise_for_status()
      if re.search('^!settopic .*', los) and re.search('mod=1',userstate) != None:
         status = los.strip('!settopic')
         status = status.strip('\r\n')
         params = 'channel[status]='+status
         requests.put(url, headers = aheaders, params = params).raise_for_status()
      if re.search('^!changesumm .*', los) and re.search('mod=1',userstate) != None:
         tmpname = los.strip('!changesumm')
         print tmpname
         tmpname = tmpname.rstrip('\r\n')
         print tmpname
         getbuffer = requests.get(baseriot+'na/v1.4/summoner/by-name/'+tmpname+apikey).text
         if re.search('[45]0[0-9]',getbuffer) != None and re.search('status_code',getbuffer) != None:
             irc.send('PRIVMSG ' +channel+' :/me SUMMONER NAME '+tmpname+ ' NOT FOUND!\r\n')
         else: 
             mysummname = tmpname
             summnametag = mysummname.replace(' ','')
             summnametag = summnametag.lower()
             mysummid = str(json.loads(getbuffer)[summnametag]['id']) 
             irc.send('PRIVMSG '+channel+' :/me Active Summoner Name changed to: '+mysummname+'\r\n')
      if re.search('^!runes', los) and runeflood < ctime:
         actpageunique = set()
         actpage = []
         actrunes = []
         runecount = []
         runename = ''
         runeflood = ctime+30
         runedb = json.loads(requests.get(baseriot+'static-data/na/v1.2/rune'+apikey).text)['data']
         getbuffer = requests.get(baseriot+'na/v1.4/summoner/'+mysummid +'/runes'+apikey).text
         runepages = json.loads(getbuffer)[mysummid]['pages']
         for page in runepages:
             #print page['current']
             if page['current'] != False:
                 actpage = (page['slots'])
                 for slot in actpage:
                     actrunes.append(slot['runeId'])
                     actpageunique.add(slot['runeId'])
         for rune in actpageunique:
             runename = runename + runedb[str(rune)]['name']+' x'+ str(actrunes.count(rune)) + ' | '
         runename = runename.rstrip('| ')
         irc.send('PRIVMSG '+channel+' :/me ' +mysummname+'\'s current runepage: ( '+ runename + ' ) \r\n')
         runedb = []
         getbuffer = []
      if re.search('^!rank', los) and rankflood < ctime:
         rankflood = ctime+30
         getbuffer = requests.get(baseriot+'na/v2.5/league/by-summoner/'+mysummid+'/entry'+apikey).text
         if re.search('[45]0[0-9]',getbuffer) != None and re.search('status_code',getbuffer) != None:
             irc.send('PRIVMSG '+channel+' :/me '+mysummname+' is unranked.\r\n')
         else:
             leagueinf = json.loads(getbuffer)[mysummid][0]
             tier = leagueinf['tier']
             div = leagueinf['entries'][0]['division']
             lp = str(leagueinf['entries'][0]['leaguePoints'])
             wins = str(leagueinf['entries'][0]['wins'])
             losses = str(leagueinf['entries'][0]['losses'])
             irc.send('PRIVMSG '+channel+' :/me '+mysummname+' - '+tier +' '+div+ ' ' +lp+' LP  ( Wins: '+ wins+ ' | Losses: '+losses+' ). \r\n')
         getbuffer = ''
