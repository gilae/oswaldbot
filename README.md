# oswaldbot

Another chatbot, this time for Twitch.tv

---

# setup

Included are 3 text files, the following is instructions on how to set them up:

## oauth.txt

The api key is retrieved from http://twitchapps.com/tmi/ ON THE BOT'S ACCOUNT

## token.txt

This token is retrieved from https://dev.twitch.tv/ ON THE CHANNEL'S ACCOUNT

## apikey.txt

This api key is for the Riot API, retrieved at https://developer.riotgames.com/api-keys.html on whatever account you'd like

---

# Running

Type ``python oswaldbot_public.py`` in the command line
